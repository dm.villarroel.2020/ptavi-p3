#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import xml.dom.minidom

scores = ["buenisimo", "bueno", "regular", "malo", "malisimo"]


def read_joke(node):
    """Given a chiste DOM node, get its score and contents

    Returns a dictionary with 'score', 'question' and 'answer' as keys.
    """
    score = node.getAttribute('calificacion')
    questions = node.getElementsByTagName('pregunta')
    question = questions[0].firstChild.nodeValue.strip()
    answers = node.getElementsByTagName('respuesta')
    answer = answers[0].firstChild.nodeValue.strip()
    contents = {'score': score, 'question': question, 'answer': answer}
    return contents


def main(path):
    """Programa principal"""
    document = xml.dom.minidom.parse(path)
    root = document.childNodes[0]
    if root.tagName != 'humor':
        raise Exception("Root element is not humor")
    jokes = document.getElementsByTagName('chiste')
    lists = {};

    for joke in jokes:
        texts = read_joke(joke)
        score = texts['score']
        try:
            lists[score].append(texts)
        except KeyError:
            lists[score] = [texts]

    for score in scores:
        if score in lists:
            for element in lists[score]:
                print(f"Calificación: {score}.")
                print(f" Respuesta: {element['answer']}")
                print(f" Pregunta: {element['question']}")
                print()


if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit(f"Usage: {sys.argv[0]} <xml_file_path>")
    main(sys.argv[1])
