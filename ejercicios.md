# Protocolos para la transmisión de audio y video en Internet
# Práctica 3. XML y JSON

**Nota:** Esta práctica se puede entregar para su evaluación como parte de
la nota de prácticas, pudiendo obtener el estudiante hasta 0.77 puntos. Para
las instrucciones de entrega, mira al final del documento. Para la evaluación
de esta entrega se valorará el correcto funcionamiento de lo que se pide y el
seguimiento de la guía de estilo de Python.

**Conocimientos previos necesarios:**

* Nociones de Python3 (primera práctica)
* Nociones de orientación a objetos en Python3 (segunda práctica)
* Nociones de XML y SMIL (presentadas en clase de teoría)

**Tiempo estimado:** 10 horas

**Repositorio plantilla:** https://gitlab.etsit.urjc.es/ptavi/2022-2023/ptavi-p3

<!--**Práctica resuelta:** https://gitlab.etsit.urjc.es/ptavi/2021-2022-resueltas/ptavi-p3-resuelta -->

**Fecha de entrega parte individual:** 27 de octubre de 2022, 23:59 (hasta ejercicio 10, incluido)

**Fecha de entrega parte interoperación:** 31 de octubre de 2022, 23:59 (ejercicio 11)


## Introducción

Python ofrece una serie de bibliotecas para manipular ficheros en XML (como SMIL) y JSON. En esta práctica, veremos cómo utilizar la biblioteca DOM (módulo `xml.dom.minidom`) y la biblioteca JSON (módulo `json`).

## Objetivos de la práctica

* Profundizar en el uso de SMIL, XML y JSON.
* Aprender a utilizar la biblioteca DOM para el manejo de XML, en particular con Python.
* Aprender a utilizar la biblioteca  JSON, en particular con Python.
* Utilizar el sistema de control de versiones git en GitLab.

## Ejercicio 1

Con el navegador, dirígete al repositorio [ptavi-p3](https://gitlab.etsit.urjc.es/ptavi/2022-2023/ptavi-p3) y realiza un fork ([instrucciones sobre cómo hacer un fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)), de manera que consigas tener una copia del repositorio en tu cuenta de GitLab. Clona en tu ordenador local el repositorio que acabas de crear a local para poder editar los archivos (usando `git clone`).

Trabaja a partir de ahora en la práctica, registrando los ficheros que vayas añadiendo (usando `git add`) y los cambios que vayas haciendo (usando `git commit`) al hacer los próximos ejercicios. Recuerda que al final del todo tendrás que subir (empujar) estos cambios al repositorio en GitLab (usando `git push`).

## Ejercicio 2

Inspecciona el fichero `chistes.py`, en este mismo directorio. Verás que el fichero consta de tres partes:

* Importación de del módulo `xml.dom.minidom`.

* La función `main`. Esta función utiliza la función `parse` de `xml.dom.minidom` para crear un DOM a partir del fichero XML `chistes.xml`, poniéndolo en la variable `document`. A continuación, obtiene la lista de elementos `chiste`, y para cada uno de ellos, obtiene el atributo `calificacion` y los elementos `pregunta` y `respuesta`. Finalmente, muestra en pantalla el contenido de estos elementos. Es interesante cómo en el caso de los elementos `pregunta` y `respuesta`, la función `getElementsByTagName` devuelve una lista de elementos (aunque sólo contenga un elemento). De ella se coge el primer elemento (el elemento 0), del que se obtiene el primer hijo (usando `first.Child`), que es también un elemento, del que se obtiene su valor como una cadena (usando `nodeValue`). Por último, de esta cadena se eleminan los espacios y fines de línea que pueda tener al principio y al final.

* La llamada a la función `main`, que solo se realiza cuando el script ha sido llamado como programa principal (en ese caso, al variable de sistema `__name__` tiene el valor `__main__`).

Ejecútalo, pruébalo en el depurador.

Incluye el programa `chistes.py`, tal y como está, en el repositorio de entrega (`git add`), y registra el cambio (`git commit`).

Una vez en el repositorio de entrega, ejecuta también sus tests:

```shell
python3 -m unittest tests/test_chistes.py
```

## Ejercicio 3

Crea un nuevo programa, `chistes2.py`, que escriba los chistes en orden inverso (primero el último que aparezca en el fichero XML), y que escriba primero las respuestas y luego las preguntas. Por lo demás, usa el mismo formato que produce `chistes.py`.

Asegúrate de que las instrucciones del programa principal están en una funcion (llamada `main`), pues así lo espera el test que este programa tendrá que pasar.

Puedes probar el programa ejecutando el test `test_chistes2.py`:

```shell
python3 -m unittest tests/test_chistes.py
```

Incluye el programa `chistes2.py` en el repositorio de entrega (`git add`), y registra el cambio (`git commit`).

## Ejercicio 4

Crea un nuevo programa, `chistes3.py`, modificando el programa `chistes.py` para que:

* La función `main` admita un parámetro, `path`, que será el nombre del fichero con el documento XML de chistes.

* Escriba los chistes en orden según su calificación. Las calificaciones posibles son (en este orden, de mejor a peor): "buenisimo", "bueno", "regular", "malo", "malisimo".

* Tras cada chiste, escribirá una línea en blanco, para mejor separar los chistes unos de otros.

* Si el elemento raíz del fichero no se llama `humor`, la función `main` debe levantar la excepción Exception con el mensaje "Root element is not humor".

Por lo demás, el programa se comportará igual que `chistes.py`, y escribirá los chistes en el mismo formato.

Puedes probar el programa ejecutando el test `test_chistes2.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 5

Crea un nuevo progarma, `chistesoo.py`. Este programa producirá exactamente los mismos resultados que `chistes3.py`. Pero para ello, utilizará una clase, `Humor`. Esta clase tendrá las siguientes funciones:

* Una función de inicialización, `__init__`, que acetpará como parámetro un nombre de fichero (`path`) con un documento XML con chistes, en el mismo formato que `chistes.xml`.

* Una función `jokes`, qur devolverá la lista de chistes que haya en el fichero XML. Cada elemento de la lista tendrá información sobre un chiste, en forma de diccionario. Las claves de estos diccionarios serán `score`, `question` y `answer`, que tendrán la calificación, la pregunta y la respuesta del chiste. La lista está en orden de calificación, según se indicaba en el ejercicio anterior (desde `buenísimo` a `malísimo`).

La función `main` instanciará un objeto de esta clase para leer el fichero cuyo nombre le pasen como parámetro (igual que en el ejercico anterior), obtener el listado de chistes en él, y mostrarlo luego en pantalla.

Por lo demás, el programa se comportará igual que `chistes3.py`, y escribirá los chistes en el mismo formato.

Puedes probar el programa ejecutando el test `test_chistesoo.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 6

Crea un fichero `smil.py`, con dos clases llamadas `SMIL` y `Element`.

La clase `Element` tendrá las siguientes funciones:

* Una función de inicialización, `__init__`, que aceptará como parámetro un nombre de elemento, un diccionario de atributos, y una cadena con el contenido del elemento:
  * El nombre de elemento será un elemento de SMIL (por ejemplo 'layout` o 'img'), aunque no se comprobará que sea un elemento válido del estándar SMIL.
  * El diccionario de atributos tendrá todos los atributos del elemento. Sus claves serán los nombres de los atributos, y sus valores, los valores de estos atributos (de tipo string).
  * El contenido del elemento será la parte del documento XML que está "dentro" del elemento.

* Una función `name`, que devolverá el nombre del elemento.
* Una función `attrs`, que devolverá el diccionario de atributos.
* Una función 'content', que devolverá el contenido del elemento, como cadena de texto (string).
* Una función 'children', con la lista de elementos que haya dentro del él (si hay elementos dentro de él).


La clase 'SMIL' tendrá dos funciones:

* Una función de inicialización, `__init__`, que acetpará como parámetro un nombre de fichero (`path`) con un documento SMIL, que habrá que analizar.

* Una función `root`, que devolverá el elemento principal, como objeto de la clase `Element`.

* Una función `elements', que devolverá una lista con todos los elementos del documento, cada uno de ellos de tipo `Elemento`, con sus datos. Esta lista tendrá los elementos en el orden en que aparecen en el documento.

Puedes probar el programa ejecutando el test `test_smil.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 8

Crea un programa `elements.py`, que tenga una función `main`, que se ejecutará cuando el programa sea llamado como módulo principal. Esta funciòn será lo único que se ejecute en ese caso (y si no se le llama como programa principal, no se ejecutará nada).

El programa aceptará un argumento de línea de comandos, que será el nombre el fichero que contenga un documento SMIL a analizar. La función main() conseguirá ese argumento, y escribirá en pantalla el nombre de todos los elementos que aparezcan en el documento, uno por líena, en el orden en que aparecen. Para ello, utilizará objetos de las clases `SMIL` y `Element` del fichero `smil.py`. Si no se encuentra un argumento de lína de comandos, y solamente uno, la función `main` terminará el programa escribiendo:

```
Usage: python3 elements.py <file> 
```

Para realizar pruebas, se puede utilizar el fichero `karaoke.smil`. Para este fichero, la salida del programa debe ser:

```
smil
head
layout
root-layout
region
region
region
body
par
img
img
audio
textstream
```

Puedes probar el programa ejecutando el test `test_elements.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 9

Crea un programa `json.py`, que tenga una función `main`, que se ejecutará cuando el programa sea llamado como módulo principal. Esta funciòn será lo único que se ejecute en ese caso (y si no se le llama como programa principal, no se ejecutará nada).

La función se comportará como la de `elements.py`, pero en lugar de escribir el listado de nombres de elementos, escribirá un documento JSON con un formato como el siguiente:

```json
{
  "name": "humor",
  "attrs": {},
  "children": [
    {
      "name": "chiste",
      "attrs": {"calificacion": "malo"},
      "children": [ ... ]
    },
    ...
  ]
}
```

Esto es, para cada elemento del documento SMIL, creará un diccionario, con las siguientes claves:

* `name`, que tendrá el nombre del elemento (de tipo `string`)
* `attrs`, que tendrá un diccionario con los atributos (con los nombres de los atributos como claves, y sus valores como varlores del diccionario), o un diccionario vacío si no hay ninguno.
* `children`, que tendrá una lista con todos los elementos hijos, en el orden en que aparezcan en el documento, o una lista vacía si no hay ninguno.

Para hacerlo, se creará la clase `SMILJSON`, que heradará de la clase `SMIL` del fichero `smil.py`, y tendrá una nueva función `json` que devolverá un string con el documento JSON correspondiente al todo el fichero con que se inicializa. Esta clase se apoyará en la clase `ElementJSON`, que heredará de la clase `Element` del fichero `smil.py`, y tendrá una nueva función `dict` que devolverá un diccionario con los datos que habrá que utlizar para componer la parte del documento JSON corresondiente a ese elemento (y tendrá por tanto las claves `name`, `attrs` y `chlidren`).

Puedes probar el programa ejecutando el test `test_elements2.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).

## Ejercicio 10

Crea otro programa `download.py`, también con su programa principal en una función `main`. Este programa aceptará un argumento en la línea de comandos, el nombre del archivo SMIL a procesar, y descargará en local el contenido multimedia remoto referenciado en ese archivo SMIL (atributo `src` de elmentos `img`, `audio` y `textstream`). De esta manera, si el atributo `src` de un elemento del archivo SMIL tiene como valor un elemento en remoto (o sea, una URL que empiece por `http://` o `https://`), se deberá descargar ese elemento a un fichero local.

Para descargar documentos correspondienets a una URL, utilizaremos la función [`urlretrieve`](https://docs.python.org/3/library/urllib.request.html#urllib.request.urlretrieve) de la biblioteca `urllib.request` de Python3.

Cada fichero que descargue lo descargará con el nombre `fichero-<n>.<ext>`, donde `fichero` será siempre el principio del nombre del fichero, `<n>` (empezando por 0) será el número de fichero que se descarga, en el orden en el que están en el fichero SMIL, y `<ext>` la extensión de la url que se descargó. Para identificar la extensión, se considerará suficiente partir la url usando el carácter punto como divisor (usando `split`) y quedarse con el último elmento de la lista resultante (quien quiera realizar una división más completa, puede usar `urlparse` y `splitext`).

Por ejemplo, si las urls son:

```
https://un.servidor.com/fichero.jpeg
http://otro.servidor.org/direct.orio/fich.2.mp3
```

los ficheros en que se crearán serán `fichero-0.jpeg` y `fichero-1.mp3`.

Puedes probar el programa ejecutando el test `test_download.py`. Cuando tengas el programa en el repositorio de entrega, añádelo para que git lo tenga en cuenta (`git add`), y registra el cambio (`git commit`).


## ¿Qué se valora de esta la práctica?

Valoraremos de esta práctica sólo lo que esté en la rama principal de
tu repositorio, creado de la forma que hemos indicado (como fork del repositorio plantilla que os proporcionamos). Por lo tanto, aségurate de que está en él todo lo que has realizado.

Además, ten en cuenta:
* Se valorará que haya realizado al menos haya seis commits, correspondientes más o menos con los ejercicios pedidos, sobre la rama principal del repositorio.
* Se valorará que el código respete lo especificado en PEP8.
* Se valorará que estén todos los archivos que se piden en los ejercicios anteriores.
* Se valorará que los programas se invoquen exactamente según se especifica, y que muestren  mensajes y errores correctamente según se indica en el enunciado de la práctica.
* Parte de la corrección será automática, así que asegúrate de que los nombres que utilizas para archivos, clases, funciones, variables, etc. son los mismos que indica el enunciado.

## ¿Cómo puedo probar esta práctica?

Para muchos de los apartados del programa, se proporciona un test. Puedes ejecutarlo para ver que el test pasa. Ten en cuenta que el hecho de que pase el test no quiere decir que la parte correspondiente esté completamente bien, aunque los tests están diseñados para probar los errores más habituales. Recuerda que si el test es `test_xxx.py`, para ejecutarlo podrás ejecutarlo en PyCharm, o desde la línea de comandos:

```shell
python3 -m unittest tests/test_xxx.py
```

Cuando tengas la práctica lista, puedes realizar una prueba general, incluyendo la comprobación del estilo de acuerdo a PEP8, que los ficheros en el directorio de entrega son los adecuados, y alguna otra comprobación. Para ello, ejecuta el archivo `check.py`, bien en PyCharm, o bien desde la línea de comandos:

```shell
python3 check.py
```

## Cómo actualizar el repositorio de la práctica si éste es actualizado

Puede ocurrir que mientras estás trabajando con tu práctica, después de haber hecho tu fork para poder trabajar clonándolo localmente, actualicemos el repositorio plantilla (del que hiciste el fork). Puedes incorporar las modificaciones al repositorio plantilla en el tuyo, de la siguiente manera.

Primero, tienes que añadir, a tu repositorio local (el que clonaste de tu fork) un nuevo `remote`. Los "remote" son los repositorios remotos con los que tu repositorio local puede sincronizarse. Tu repositorio tiene un remote (llamado `origin`) que corresponde con tu repositorio en GitLab (el que creaste como fork del repositorio plantilla). Puedes ver los remotes de tu repositorio:

```shell
git remote -v
```

Verás dos líneas para `origin`, una para `fetch` (recibir datos) y otra para `push` (enviar datos).

Para poder actualizar a partir del repositorio plantilla, vamos a añadir un nuevo remote para él:

```shell
git remote add upstream https://gitlab.etsit.urjc.es/ptavi/2021-2022/ptavi-p3-21
```

Si ahora vuelves a ejecutar `git remote -v` verás cuatro líneas, dos para el remote `origin` y dos para el nuevo remote `upstream`.

En este momento es importante comprobar si tienes pendientes modificaciones a ficheros que conoce git (esto es, hay modificaciones que aún no han sido incluidas en ningún commit). Para ello, puedes comprobar que `git status` está limpio, o puedes tratar de realizar un commit que incluya todos los cambios pendintes (si no hay cambios git te lo dirá). Para tratar de haer este commit puedes escribir, en el directorio principal de tu repositorio (atención al `.` final):

```shell
git commit .
```

Ahora que ya tenemos al repositorio plantilla como remote, y estamos seguros de que no hay cambios pendientes, podemos sincronizar con él. Si no has tocado los ficheros que obtuviste del repositorio, git se encargará de gestionar la sincronización completamente. Si los has tocado, puede ser un poco más complicado, pero posible. Por si acaso, antes de realizar los siguientes pasos, copia los ficheros que hayas escrito (tus programas, fundamentalmente) a otro directorio, por si acaso.

La forma más simple de sincronizar los contenidos del repositorio plantilla es la siguiente:

```shell
git fetch upstream
git rebase upstream/master
```

La primera orden bajará la informacion nueva que pueda haber en el remote `upstream` (el reposito plantilla de la practica), pero no la incorporara en el directorio. La segunda orden incorporara los commits nuevos que pueda haber en la rama principal de `upstream` en la rama en la que estés trabajando en tu repo local. Para hacerlo, `git rebase` hará comprobaciones, para asegurarse de que esa incorporación es segura, sin perder información. Además, colocará los commits que tengas pendientes en tu rama detrás de los commits que descargue de `upstream/master`.

Si `git rebase` falla, porque no puede asegurar que la incorporación de commits es segura, te lo dirá, dándote varias opciones, ente ella la de abortar. Si es el caso, trata de entender la situacuón, y en caso de duda, aborta. Probablmente tengas cambios que den conflicto, o tengas cambios si incluir en un commit. Si `git rebase` tiene éxito, ya tienes en tu repositorio local los cambios que se hayan hecho en el repositorio plantilla de la práctica.

Alternativamente a `git rebase` puedes usar `git merge`, que hará algo muy parecido, pero manteniendo la historia de commits de otra forma:

```shell
git fetch upstream
git merge upstream/master
```

Si por algún motivo estás trabajando en una rama distinta de la principal, y quieres incorporar los cambios a la principal, asegúrate que pasas a esa rama principal antes, con `git checkout master`.


## Ejercicio 11 (segundo periodo)

Para realizar esta práctica, tendrás que utilizar el grupo de GitLab que creaste en el último ejercico de la prática 2 (y que llamaste `forks-<user>`). Si no lo has hecho todavía, tendrás que hacer al menos su primera parte (hasta que tengas creado el grupo).

Una vez haya terminado el periodo de entrega de las prácticas, y hayas creado tu grupo para forks, busca entre los forks del [repositorio plantilla de esta práctica](https://gitlab.etsit.urjc.es/ptavi/2022-2023/ptavi-p3) uno de otro alumno que la haya entregado (vamos a llamar `alumno` al identificador en el GitLab de la ETSIT de ese alumno). Para buscar entre los forks, cuando estés ya en el repositorio plantilla, pulsa sobre el número que aparece a la derecha de "Fork", arriba a la derecha.

Cuando hayas elegido el repositorio de `alumno`, haz un fork de él, indicando que quieres que el fork sea sobre el grupo de forks que has creado (`forks-<user>`). Para no confundirte, usa un nombre distinto para el nuevo  repositorio: `ptavi-p3-<alumno>`.

Crea un clon local de este nuevo repositorio (usando `git clone`), y crea en él una rama nueva de desarrollo, a partir de la rama principal, en la que vas a implementar la funcionalidad de este segundo periodo. Puedes ver el proceso para hacerlo (y para cambiarte de rama) en el enunciado del último ejercico de la práctica 2.

En la rama `nueva` tendrás todos los ficheros que entregó tu compañero. Añade un nuevo fichero, `karaoke-<user>.smil`, que sea un fichero smil válido (basta con que modifiques levemente el fichero `karaoke.smil`. A continuación, en el directorio `tests` crea un nuevo test, `test_<user>.py` que compruebe que la función `main` del programa `json.py` de `alumno` funciona correctamente con el nuevo fichero que acabas de crear, `karaoke-<user>.smil`. Esto es, que produce el fichero JSON adecuado cuando se invoca así:

```shell
json.py karaoke-<user>.smil
```

Cuando termines, crea un commit (usando `git add` y `git commit`, o desde Pycharm) que inclya estos cambios. Sincroniza tus ficheros en esta nueva rama al repositorio que creaste al principio de este ejercicio (en tu grupo de forks)). Puedes suar  usando `git push` cuando estás en la rama `nueva`.

A continuación, realiza un "merge request" (solicitud de añadir tu cambio a su código) al repositorio de `alumno` (puedes consultar la [ayuda en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork)).

Si recibes en tu repositorio solicitudes de añadir un cambio ("merge requests") de otros compañeros, acepta todos los que puedas, si los nombres de los archivos que proponen añadir son los correctos, y no hacen más cambios a tu repositorio que añadir esos archivos ([documenación en el manual de GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)).
