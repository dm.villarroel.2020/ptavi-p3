#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from os.path import splitext
from urllib.parse import urlparse
from urllib.request import urlretrieve

import smil


def retrieve(url: str, n: int) -> bool:
    """Retrieve url and store it in file<n>.<ext>

    Retrieves only if url starts with http:// or https://.
    Returns True if retrieved, False otherwise.
    <ext> is the extension of the url, if any.
    """
    if url.startswith('http://') or url.startswith('https://'):
        path = urlparse(url).path
        ext = splitext(path)[1]
        filename = f'fichero-{n}{ext}'
        print(f"Retrieving {url} into {filename}...")
        urlretrieve(url, filename)
        return True
    else:
        return False


def download(els):
    count = 0
    for element in els:
        attrs = element.attrs()
        for name in attrs:
            if name == 'src':
                result = retrieve(attrs[name], count)
                if result:
                    count += 1


def main(path):
    """Programa principal"""
    karaoke = smil.SMIL(path)
    elements = karaoke.elements()
    download(elements)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit("Usage: python3 download.py <file>")
    main(sys.argv[1])

