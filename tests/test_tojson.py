#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import json
import os
import unittest

import tojson

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

result_karaoke = [{'attrs': {}, 'name': 'smil'},
 {'attrs': {}, 'name': 'head'},
 {'attrs': {}, 'name': 'layout'},
 {'attrs': {'background-color': 'blue', 'height': '300', 'width': '248'},
  'name': 'root-layout'},
 {'attrs': {'id': 'a', 'left': '64', 'top': '20'}, 'name': 'region'},
 {'attrs': {'id': 'b', 'left': '20', 'top': '120'}, 'name': 'region'},
 {'attrs': {'id': 'text_area', 'left': '20', 'top': '100'}, 'name': 'region'},
 {'attrs': {}, 'name': 'body'},
 {'attrs': {}, 'name': 'par'},
 {'attrs': {'begin': '2s',
            'dur': '36s',
            'region': 'a',
            'src': 'http://www.content-networking.com/smil/hello.jpg'},
  'name': 'img'},
 {'attrs': {'begin': '12s',
            'end': '48s',
            'region': 'b',
            'src': 'http://www.content-networking.com/smil/earthrise.jpg'},
  'name': 'img'},
 {'attrs': {'begin': '1s',
            'src': 'http://www.content-networking.com/smil/hello.wav'},
  'name': 'audio'},
 {'attrs': {'fill': 'freeze',
            'region': 'text_area',
            'src': 'http://gsyc.es/~grex/letra.rt'},
  'name': 'textstream'},
 {'attrs': {'begin': '4s', 'src': 'cancion.ogg'}, 'name': 'audio'}]

class TestMain(unittest.TestCase):

    def setUp(self):
        os.chdir(parent_dir)
        self.stdout = StringIO()

    def test_json(self):
        with contextlib.redirect_stdout(self.stdout):
            tojson.main(os.path.join("karaoke.smil"))
        output = self.stdout.getvalue()
        output_list = json.loads(output)
        self.assertEqual(result_karaoke, output_list)



if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
