#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

import smil

def main(path):
    """Programa principal"""

    karaoke = smil.SMIL(path)
    els = karaoke.elements()
    names = [el.name() for el in els]
    for name in names:
        print(name)

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 elements.py <file>")
    main(sys.argv[1])
